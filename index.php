<!DOCTYPE html>
<html lang="ua">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Туроператор TUI Україна. Подорожуйте по світу разом з TUI!</title>

	 <link rel="shortcut icon" href="new-landing/img/favicon.ico" type="image/x-icon">

	<!-- Select2 css -->
	<link rel="stylesheet" href="new-landing/libs/select2-master/select2.min.css">
	
	<!-- Bootstrap -->   
	<link href="new-landing/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Style -->  
	<link href="new-landing/style/style.css" rel="stylesheet">

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PVGRJTZ');</script>
	<!-- End Google Tag Manager -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body id="up">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PVGRJTZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


	<section id="header-lend">
		<div class="container">
			<div class="row padding top-nav">
				<div class="nav-container">
		  			<div class="col-xs-4 col-sm-2 col-lg-2"><a href=#up><img src="new-landing/img/logo.png" alt="logo" class="logo" /></a></div>
		  			<div class="col-xs-push-6 col-xs-2 col-sm-push-0 col-sm-7 col-lg-8 nav">
			  			<div class="botton_nav">			  				
			  				<div class="nav_mob_blok scroll" id="scroll-nav-mob">
								<span><a href="#popularini_tours_in_turechchin">Готелі</a></span>
				  				<span><a href="#start-document">Про компанію</a></span>
				  				<span><a href="#advantages">Переваги</a></span>
				  				<span><a href="#popular-resorts">Курорти</a></span>
			  				</div>			  				
			  			</div>
		  			</div>
		  			<div class="col-xs-pull-2 col-xs-6 col-sm-pull-0 col-sm-3 col-lg-2 nav-tel">
		  				<a class="tel tel-desc"><img src="new-landing/img/mobile-fon.png" /><span> 0 800 500 394</span></a>
		  				<a href="tel:0800500394" class="tel tel-mob"><img src="new-landing/img/mobile-fon.png" /><span> 0 800 500 394</span></a>
		  			</div>
		  		</div>
			</div>
			<div class="header-text">
				<div class="row">  			
		  			<div class="col-sm-12">
			  			<h1>Швидкий підбір турів в Єгипет<br/>
						спеціалістами TUI</h1>
			  		</div>
			  		<div class="col-sm-12">
			  			<p>Не гайте часу. Ми це зробимо за Вас.</p>
			  		</div>
					<div class="row headerInput col-sm-12">
						<div class="input-group">
							<input type="text" class="form-control" name="userName" id="userName" placeholder="Ваше ім'я" size="30">
						</div>
						<div class="input-group">
							<input type="text" class="form-control" name="userPhone" id="userPhone" placeholder="Ваш номер телефону" size="30">
						</div>
					</div>
					<div class="row btnSend">
						<div class="col-sm-12 col-md-12">
							<button class="btn red-button" id="smallFormHeader">Відправити запит</button>		
						</div>
					</div>		  			
		  		</div>
			</div>
		</div> 	
	</section>

<!--    
	<section id="all-on-the-sea">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
		    		<h2>АКЦІЯ “ВСІ НА МОРЕ!”</h2>
		    	</div>
		    </div>
		</div>	 	
	</section>
	
	<section id="section-blue-text">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="blue-text">
		    		<p>Ми знизили ціни на тури на травневі свята і підготували для Вас приємні подарунки: безкоштовне харчування на борту, індивідуальні трансфери, вечері та інше. В акції беруть участь рекомендовані готелі для відпочинку в сонячній</p>
		    		</div>
		    	</div>
		    </div>
		</div>	 	
	</section>
-->
	<section id="popularini_tours_in_turechchin">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
		    		<h2>Тури в Єгипет від 8765 грн</h2>
		    	</div>
		    </div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Sharming-Inn.jpg"/>
								</div>
							</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Sharming Inn 4*</div>
							<div class="p-about-tour">
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Вигідна пропозиція</p>
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Універсальний відпочинок </p> 
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Власний пляж </p> <br>
								<p>&nbsp;</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 8765 </span><span class="from-price">грн за одну людину </span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Хургада
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/AMC-Royal-Hotel.jpg"/>
								</div>
							</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">AMC Royal Hotel 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Вигідна пропозиція</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Універсальний відпочинок для молоді та дорослих</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Перша лінія</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Власний пляж</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Водні горки</p> 
								<p>&nbsp;</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 10616 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>	
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Royal-Grand-Sharm.jpg"/>
								</div>
															</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Royal Grand Sharm 5*</div>
							<div class="p-about-tour">
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Гарне співвідношення ціна якість</p>
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Поруч інфраструктура</p> 
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Перша лінія</p> 
								<p class="span-about-tour"><i class="fa fa-check" aria-hidden="true"></i> Кораловий риф</p> 
								<p>&nbsp;</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 12050 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								 Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Amwaj-Oyoun-Resort.jpg"/>
								</div>
															</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Amwaj Oyoun Resort & Spa 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Вигідна пропозиція</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Універсальний відпочинок </p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Перша лінія</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Власний пляж</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Поруч інфраструктура</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 10809 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>	
	</section>
	
	<section class="more-ukrainian">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
		    		<h2>МИ ОСОБИСТО ПЕРЕГЛЯНУЛИ ГОТЕЛІ</h2>
		    	</div>
		    	<div class="col-xs-12">
		    		<p>щоб відібрати для вас найкраще, що є в Єгипті</p>
		    	</div>		    	
		    </div>
		</div>	 
	</section>
	
	<section id="online-selection-tour">		
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Хургада
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Mercure-Hurghada-Hotel.jpg"/>
								</div>
															</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Mercure 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Якісний готель для сімейного відпочиноку </p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Велика територія </p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Стильні номери</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Кораловий риф</p> <br>
                                <p>&nbsp;</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 12129 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-xs-12 col-md-6 my-form">
				
					<div class="online-selection-tour-form">
							<div class="row box-content" id="online-selection-form">

								<div class="col-xs-12">
									<div class="h2-form">Будь-ласка, заповніть<br> форму</div>
									<div class="p-form">Ми швидко Вам зателефонуємо</div>
								</div>
								<div class="col-xs-6">
									<div class="input-placeholder">Ваше ім'я</div>
									<input type="text" name="country" id="country" placeholder="Ваше ім'я" size="60" onkeyup="placeholderInput($(this))">
									<div class="form-error errorName">Введіть Ваше ім`я</div>
								</div>
								<div class="col-xs-6">
									<div class="input-placeholder">Бюджет</div>
									<input type="text" name="budget" id="budget" placeholder="Бюджет" size="60" onkeyup="placeholderInput($(this))">
								</div>								
								<div class="col-xs-6">
									<div class="input-placeholder">Кількість людей</div>
									<input type="text" name="number_people" id="number_people" placeholder="Кількість людей" size="40" onkeyup="placeholderInput($(this))">
								</div>
								<div class="col-xs-6">
									<div class="input-placeholder">Кількість днів</div>
									<input type="text" name="number_days" id="number_days" placeholder="Днів" size="40" onkeyup="placeholderInput($(this))">
								</div>
								<div class="col-xs-12">
									<div class="input-placeholder">Введіть Ваш Телефон</div>
									<input type="text" name="phone" id="phone" placeholder="Телефон" size="60" onkeyup="placeholderInput($(this))">
									<div class="form-error errorPhone">Неправильний формат телефону</div>
								</div>
								<div class="col-xs-6">
									<div class="input-placeholder">Введіть Вашу Почту</div>
									<input type="text" name="email" id="email" placeholder="E-mail" size="60" onkeyup="placeholderInput($(this))">
									<div class="form-error erroreMail">Введіть правильну e-mail адресу</div>
								</div>
								<div class="col-xs-6"><span class="">
									<div class="input-placeholder z-index">Місто</div>
									<select class="js-example-basic-single-limit" id="select-town">
										<option></option>																	
									</select>
									<div class="form-error erroreSelect">Оберіть Ваш Місто</div>
								</div>
								<div class="col-xs-12">
									<input type="button" class="red-button call_modal" name="" value="Відправити">
								</div>
								<div class="col-xs-12">
					<div class="online-selection-checkbox" id="selection-check-error">
		<input type="checkbox" name="online-selection" class="selection-form-checkbox" id="online-selection-check" checked>
		<label for="online-selection-check" class="p-form-checkbox"><span class="span-form-checkbox">Надаю згоду на обробку моїх персональних даних згідно Закону України «Про захист персональних даних»</span></label>
					</div>
								</div>
					</div>					
				</div>
				</div>
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Хургада
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Stella-Di-Mare-Resort-&-Spa-Makadi.jpg"/>
								</div>
				            </div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Stella Di Mare Resort & Spa Mak</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Спокійний сімейний відпочинок</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Кораловий риф</p>
								<br /><p>&nbsp;</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 12149 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Sea-Club-Resort.jpg"/>
								</div>
							</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Sea Club Resort 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Сімейний та молодіжний відпочинок </p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Велика територія</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Якісний номерний фонд</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Професійні вечірні шоу</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Кораловий риф</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 12257 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>	
	</section>
	
		<section id="five-reasons">
		<div class="text-five-reasons" id="start-document">
			<div class="container">
				<div class="row">
				<div class="col-xs-12"><h2>5 ПРИЧИН ДОВІРИТИ ВІДПОЧИНОК TUI</h2></div>
				<div class="col-xs-12 col-sm-4 col-blue-circle">
					<div class="blue-circle"><img src="new-landing/img/icons/1.svg" class="blue-img"/></div>
					<div class="text-top">
					<h4>Фінансова стабільність</h4>
					<p>Міжнародний туроператор</p>
					<p> зі світовим ім’ям</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-blue-circle">
					<div class="blue-circle"><img src="new-landing/img/icons/2.svg" class="blue-img"/></div>
					<div class="text-top">
					<h4>Європейські цінності</h4>
					<p>Якісний сервіс на</p>
					<p>кожному етапі подорожі</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-blue-circle">
					<div class="blue-circle"><img src="new-landing/img/icons/3.svg" class="blue-img"/></div>
					<div class="text-top">
					<h4>Есклюзивний продукт</h4>
					<p>Раніше доступний лише</p>
					<p> європейським туристам</p>
					</div>
				</div>
				<div class="col-xs-12 col-xs-offset-0 col-sm-offset-2 col-sm-4 col-lg-offset-0 col-blue-circle">
					<div class="blue-circle"><img src="new-landing/img/icons/4.svg" class="blue-img"/></div>
					<div class="text-top">
					<h4>Підтримка</h4>
					<h4> 24/7 </h4>
					<p>Цілодобова служба</p>
					<p> підтримки клієнтів</p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-4 col-blue-circle">
					<div class="blue-circle"><img src="new-landing/img/icons/5.svg" class="blue-img"/></div>
					<div class="text-top">
					<h4>250 TUI </h4>
					<h4>турагенцій</h4>
					<p>по всій території</p>
					<p>України</p>
					</div>
				</div>
				</div>
			</div>
		</div>
    </section>
	
	<section class="more-ukrainian">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
		    		<h2>БІЛЬШЕ <span>500 000 УКРАЇНЦІВ</span></h2>
		    	</div>
		    	<div class="col-xs-12">
		    		<p>
		    		<span>обрали відпочинок в Єгипті в 2016 році,</span>
		    		<span> 85% рекомендували б знайомим відвідати країну</span>		    			
		    		</p>
		    	</div>		    	
		    </div>
		</div>	 
	</section>
	
	<section id="popularini_tours_in_turechchin">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								  Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Iberotel-Il-Mercato.jpg"/>
								</div>
															</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Iberotel Il Mercato 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Гарне співвідношення ціна якість </p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Стильний та новий готель</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Поруч інфраструктура</p> 
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 10390 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Concorde-El-Salam-Front-Area.jpg"/>
								</div>
							</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Concorde El Salam Front Area 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Якісний готель для універсального відпочинку</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Перша лінія</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Поруч SOHO Square</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Кораловий риф</p> 
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 17683 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>	
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Шарм-ель-Шейх
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Albatros-Aqua-Park-Sharm.jpg"/>
								</div>
							</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Albatros Aqua Park Sharm 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Молодіжний відпочинок</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Власний пляж</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Безкоштовний аквапарк</p> <br>
								<p>&nbsp;</p>
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 13085  </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
				<div class="col-xs-12 col-md-6">
				
					<div class="about-tour">
					
						<div class="header-owl-carousel">
							<div class="tour-name">
								Хургада
							</div>
							<div>
								<div class="owl-carousel-slide">
								<img src="new-landing/photohotel/Jaz-Makadi-Saraya-Resort.jpg"/>
								</div>
							</div>
						</div>
						<div class="text-about-tour">
							<div class="header-about-tour">Jaz Makadi Saraya Resort 5*</div>
							<div class="p-about-tour">
								<p><i class="fa fa-check" aria-hidden="true"></i> Відома європейська мережа</p>
								<p><i class="fa fa-check" aria-hidden="true"></i> Велика зелена територія</p> 
								<p><i class="fa fa-check" aria-hidden="true"></i> Піщаний пляж з кораловим рифом</p> 
							</div>
							<div class="price"><span class="from-price">від</span><span class="tour-price"> 13805 </span><span class="from-price">грн за одну людину</span></div>
							<div class="price-button"><button class="openForm red-button">Підібрати тур</button></div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>	
	</section>
	
	<section class="online-selection">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 blue-fon">
					<p class="head-online-selection">ОНЛАЙН ПІДБІР</p>
					<p class="p-online-selection">Заповніть форму і наші менеджери підберуть найкращі готелі</p>
					<p class="p-online-selection">під Ваші вимоги для комфортного відпочинку</p>
					<a class="openTourForm white-button">Підібрати тур</a>
				</div>
			</div>
		</div>
	</section>
	
	<section id="advantages">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p class="head-advantages-text">12 ПЕРЕВАГ ВІДПОЧИТИ В ЕГИПТІ</p>
				</div>
			</div>
			<div class="row text-advantages">
				<div class="col-xs-4 col-sm-2">				
					<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_01.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">ВІЧНЕ ЛІТО</p>
				<p class="p-advantages">Тепле море та літо серед зими – комфортний клімат в Єгипті гарантує чудовий відпочинок.</p>
				</div>
				<div class="col-xs-4 col-sm-2">
					<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_04.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">НАЙГАРНІШЕ МОРЕ</p>
				<p class="p-advantages">Екологічно чисте, прозоре, унікальне, з кораловими рифами та неймовірним внутрішнім світом.</p>
				</div>
			</div>
			<div class="row text-advantages">
				<div class="col-xs-4 col-sm-2">
					<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_02.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">ВИГІДНА ЦІНА</p>
				<p class="p-advantages">Привітна країна для українського туриста – чудове співвідношення якості та ціни за тур.</p>
				</div>
				<div class="col-xs-4 col-sm-2">
					<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_05.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">РІЗНОМАНІТНІСТЬ ГОТЕЛІВ</p>
				<p class="h3-advantages">ТА СИСТЕМА «ВСЕ ВКЛЮЧЕНО»</p>
<p class="p-advantages">Широкий вибір готелів для різного стилю, де їжа та напої доступні в необмеженій кількості. </p>
				</div>
			</div>
			<div class="show-more-body">
				<div class="row text-advantages">
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_14.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">КУЛЬТУРНЕ БАГАТСТВО</p>
				<p class="p-advantages">Грандіозні пам'ятки, яким більше 40 століть. Можливість доторкнутися до  історії людства.</p>
					</div>
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_15.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">ЕКСКУРСІЇ</p>
	<p class="p-advantages">Джип-сафарі, піраміди, прогулянки Нілом, гора Мойсея та безліч інших незабутніх пригод.</p>
					</div>
				</div>
				<div class="row text-advantages">
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_09.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">ДАЙВІНГ </p>
				<p class="p-advantages">Пірнути з маскою в Червоне море – справжній must have. Краса моря здивує  як новачків, так і професіоналів. </p>
					</div>
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_13.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">СІМЕЙНИЙ ВІДПОЧИНОК</p>
	<p class="p-advantages">Піщані пляжі, корисні продукти та чудова інфраструктура роблять відпочинок в дітьми вигідним та зручним. </p>
					</div>
				</div>
				<div class="row text-advantages">
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_06.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">БЕЗЛІЧ РОЗВАГ </p>
				<p class="p-advantages">Дітям – веселі аніматори, дорослим –розважальні програми, дискотеки і бари на будь-який смак.</p>
					</div>
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_08.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">ТУРБОТА ПРО ДІТЕЙ </p>
	<p class="p-advantages">В дитячих клубах малеча оточена турботою дбайливих педагогів та з користю проводить час.</p>
					</div>
				</div>
				<div class="row text-advantages">
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_11.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">НАЦІОНАЛЬНА ЇЖА</p>
				<p class="p-advantages">Скуштуйте страви фараонів та традиційні напої в затишному ресторані або кафе біля моря.</p>
					</div>
					<div class="col-xs-4 col-sm-2">
						<div class="blue-circle"><img src="new-landing/img/icons/icon_utp_12.svg" class="blue-img"/></div>
					</div>
					<div class="col-xs-8 col-sm-4">
					<p class="h3-advantages">ЦІКАВИЙ ШОПІНГ</p>
	<p class="p-advantages">На ринках можна придбати одяг, косметику, посуд, прикраси, спеції, папірус. Головне – торгуватися!</p>
					</div>
				</div>
			</div>
				
			<div class="row text-center-show-more">
				<div class="col-xs-12">
					<a class="show-more-title grey-button">Показати ще 8 переваг</a>
				</div>
			</div>	
		</div>
	</section>
	
	<section id="popular-resorts">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<p class="head-popular-resorts">популярні курорти</p>				
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Шарм-ель-Шейх 
							</div>
							<img src="new-landing/kurort/1.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>Королівська затока, де комфорт поєднується з неймовірною красою підводного світу і золотими пісками Єгипту. Динамічний та сучасний курорт, де є все для супер відпочинку!</p>
							<a class="openFormResort">Ціни та рейси</a>					
						</div>
					</div>
				</div>
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Хургада
							</div>
							<img src="new-landing/kurort/2.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>Найвідоміший та найстаріший курорт з пологими піщаними пляжами та приємним кліматом. Рай для сімейних канікул з безліччю розваг та широким вибором готелів різного класу.</p>
							<a class="openFormResort">Ціни та рейси</a>							
						</div>
					</div>
				</div>
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Макаді Бей
							</div>
							<img src="new-landing/kurort/3.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>Молодий район, який перетворився з пустелі у чудовий та затишний курорт, вирізняється помірним ритмом відпочинку. </p>
							<p>&nbsp;</p>
							<a class="openFormResort">Ціни та рейси</a>							
						</div>
					</div>
				</div>
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Саль Хашиш
							</div>
							<img src="new-landing/kurort/4.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>«Зелена долина» з розкішними готелями, гольфовими полями, вражаючими комплексами та унікальною інфраструктурою.</p>
							<a class="openFormResort">Ціни та рейси</a>							
						</div>
					</div>
				</div>
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Сафага
							</div>
							<img src="new-landing/kurort/5.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>Курорт для гурманів, місто з нескінченними чистими пляжами, лікувальним піском, прозорим морем і веселощами.</p>
							<a class="openFormResort">Ціни та рейси</a>							
						</div>
					</div>
				</div>
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Сома Бей
							</div>
							<img src="new-landing/kurort/6.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>Затишний курорт для тих, хто втомився від галасу. Насолоджуйтеся відпочинком серед старовинної пустелі і гір.</p>
							<a class="openFormResort">Ціни та рейси</a>							
						</div>
					</div>
				</div>
				<div class="col-xs-12  col-sm-6 col-md-6 col-lg-4 hidden-about-tour">
					<div class="about-tour">
						<div class="header-popular-resorts">
							<div class="tour-name">
								Ель-Гуна
							</div>
							<img src="new-landing/kurort/7.jpg"/>
						</div>
						<div class="text-popular-resorts">
							<p>Через канали та невеличкі вілли престижний курорт називають «єгипетською Венецією». Справжній рай Червоного моря!</p>
							<a class="openFormResort">Ціни та рейси</a>							
						</div>
					</div>
				</div>
							<div class="row text-center-popular-resorts">
				<div class="col-xs-12 text-center">
					<a class="grey-button popular-resorts-title">Показати всі курорти</a>
				</div>
			</div>				
			
		</div>						
	</div>
	</section>
	
	<section class="online-selection">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 blue-fon">
					<p class="head-online-selection">консультація експерта</p>
					<p class="p-online-selection">Дізнайтесь відповіді на питання та отримайте</p>
					<p class="p-online-selection">персональний список кращих готелів та цін</p>
					<a class="openFormConsultation white-button">Допомога в підборі туру</a>
				</div>
			</div>
		</div>
	</section>
	
	<section id="advantages">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="head-advantages-text">ТУРИСТУ НА ЗАМІТКУ</p>
				</div>
			</div>
			<div class="row text-advantages">
				<div class="col-xs-4 col-sm-2">				
					<div class="blue-circle"><img src="new-landing/img/icons/20.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">МІСЦЕВІ РИНКИ</p>
				<p class="p-advantages">На місцевих ринках радимо торгуватися – так ціну можна знизити в два або три рази.</p>
				</div>
				<div class="col-xs-4 col-sm-2">
					<div class="blue-circle"><img src="new-landing/img/icons/17.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">В ЧОМУ ПЛАВАТИ?</p>
				<p class="p-advantages">Використовуйте спеціальне взуття для плавання. Його можна придбати в місцевих магазинах.</p>
				</div>
			</div>
			<div class="row text-advantages">
				<div class="col-xs-4 col-sm-2">
					<div class="blue-circle"><img src="new-landing/img/icons/18.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">БУДЬТЕ ОБЕРЕЖНІ</p>
				<p class="p-advantages">Не всі мешканці моря невинні. Особливу небезпеку несуть підводні їжаки, яких краще остерігатися.</p>
				</div>
				<div class="col-xs-4 col-sm-2">
					<div class="blue-circle"><img src="new-landing/img/icons/19.svg" class="blue-img"/></div>
				</div>
				<div class="col-xs-8 col-sm-4">
				<p class="h3-advantages">ПОДОРОЖІ</p>
<p class="p-advantages">Вигідно та зручно пересуватися курортами на таксі. Радимо обирати прогулянки зранку або ввечері.</p>
				</div>
			</div>
		</div>
	</section>
	
	<section id="advice-expert" class="online-selection">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12 blue-fon">
					<p class="head-online-selection">завітайте в найближчий офіс</p>
					<p class="p-online-selection font-size-22">250 офісів по всій Україні</p>
				</div>
			</div>
		</div>
	</section>
	
	<section id="map">
		<div class="container">
			<div class="row flex">
				<div class="col-sm-12 col-lg-4 div-map">
					<div class="row">
						<div class="col-sm-5 col-lg-6">
						<img src="new-landing/img/logo-turkey.png" alt="logo-map" class="logo-map" />
						</div>
						<div class="col-sm-7 col-lg-12">
						<p class="text-map">Cьогодні мережа TUI Tурагенцій нараховує більше ніж 250 офісів в 60 містах України</p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-lg-8">
					<img src="new-landing/img/map.png" alt="map" class="map"/>
				</div>
			</div>
		</div>
	</section>
	
	<section id="section-fotter">
		<div class=" container-fluid blue-fon">
			<div class="container">
				<div class="row div-section-fotter box-content" id="Form-section-fotter">
					<div class="col-xs-12">
						<p class="head-online-selection">ОТРИМАЙ ІНДИВІДУАЛЬНИЙ ПІДБІР ТУРУ</p>
						<p class="p-online-selection font-size-22">Ми швидко Вам зателефонуємо</p>
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-4">
						<div class="col-md-2-5 col-md-2-left">
						<div class="input-placeholder">Ваше ім'я</div>
						<input type="text" name="country" id="country" placeholder="Ваше ім'я" size="60" onkeyup="placeholderInput($(this))">
						<div class="form-error errorName">Введіть Ваше ім`я</div>
						</div>
						<div class="col-md-2-5 col-md-2-right">
							<div class="input-placeholder">Бюджет</div>
							<input type="text" name="budget" id="budget" placeholder="Бюджет" size="40" onkeyup="placeholderInput($(this))">
						</div>						
						<div class="input-emeil-placeholder">
						<div class="input-placeholder">Введіть Ваш Телефон</div>
						<input type="text" name="phone" id="phone" placeholder="Телефон" size="60" onkeyup="placeholderInput($(this))">
						<div class="form-error errorPhone">Неправильний формат телефону</div>
						</div>						
					</div>
					<div class="col-xs-12 col-sm-6 col-lg-5">
						<div class="col-md-2-5 col-md-2-left">
							<div class="input-placeholder">Кількість людей</div>
							<input type="text" name="number_people" id="number_people" placeholder="Кількість людей" size="60" onkeyup="placeholderInput($(this))">
						</div>
						<div class="col-md-2-5 col-md-2-right">
							<div class="input-placeholder">Кількість днів</div>
							<input type="text" name="number_days" id="number_days" placeholder="Днів" size="40" onkeyup="placeholderInput($(this))">
						</div>
						
						<div class="col-md-2-5 col-md-2-left">
						<div class="input-placeholder">Введіть Вашу Почту</div>
						<input type="text" name="email" id="email" placeholder="E-mail" size="60" onkeyup="placeholderInput($(this))">
						<div class="form-error erroreMail">Введіть Вашу електрону адресу</div>
						</div>
						<div class="col-md-2-5 col-md-2-right">
							<div class="input-placeholder z-index">Місто</div>
							<select class="js-example-basic-single-limit" id="select-town">
								<option></option>																	
							</select>
							<div class="form-error erroreSelect">Оберіть Ваш Місто</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-12 col-lg-3">
						<input type="button" class="red-button call_modal" name="" value="Відправити">
					</div>
					<div class="col-xs-12">
					<div class="section-fotter-checkbox" id="selection-check-error">
		<input type="checkbox" name="section-fotter" class="selection-form-checkbox" id="section-fotter-selection-check" checked><label for="section-fotter-selection-check" class="p-form-checkbox"><span class="span-form-checkbox">Надаю згоду на обробку моїх персональних даних згідно Закону України «Про захист персональних даних»</span></label>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
<div class="g-hidden">

	<div class="box-modal box-content" id="Form">
		<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i></div>
		<div class="header-form">отримати рейси та ціни</div>
		<div class="about-header-form hotel-name"></div>
		<div class="div-input">
			<div class="input-placeholder">Ім'я</div>
			<input type="text" name="name" id="name" placeholder="Ім'я" size="60" class="input-text" onkeyup="placeholderInput($(this))"/>
			<p id="name-error">Як до Вас звертатись</p>
		</div>
		<div class="div-input">
			<div class="input-placeholder">Введіть Ваш Телефон</div>
			<input type="text" name="phone" id="phone" placeholder="Введіть Ваш Телефон" size="20" class="input-text" onkeyup="placeholderInput($(this))" />
			<p id="phone-error">Наприклад (012) 345-07-89</p>
		</div>
		<div class="div-input">
			<div class="input-placeholder">Введіть Вашу Почту</div>
			<input type="text" name="email" id="email" placeholder="E-mail" size="20" class="input-text" onkeyup="placeholderInput($(this))" />
			<!-- <div class="form-error erroreMail">Введіть Вашу електрону адресу</div> -->
			<p id="email-error">Наприклад exaple@mail.com</p>
		</div>
		<div class="div-input">
			<div class="input-placeholder z-index">Місто</div>
			<select class="js-example-basic-single-limit" id="select-town">
				<option></option>																	
			</select>
			<p class="form-error erroreSelect">Оберіть Ваше місто</p>
		</div>		
		<input type="button" class="red-button call_form" name="" value="Відправити">
		<div class="checkbox" id="check-error">
		<input type="checkbox" name="Form" class="form-checkbox" id="check" checked><label for="check" class="p-form-checkbox"><span class="span-form-checkbox">Надаю згоду на обробку моїх персональних даних згідно Закону України «Про захист персональних даних»</span></label>
		</div>
	</div>

</div>

<div class="g-hidden">

	<div class="box-modal box-content" id="selectionTourForm">

			<div class="box-modal_close arcticmodal-close"><i class="fa fa-times"></i></div>
			<div class="header-form">Будь-ласка, заповніть<br> форму</div>
			<div class="about-header-form">Ми швидко Вам зателефонуємо</div>


			<div class="box-modal-block-form">
				<div class="div-input col-form">
					<div class="input-placeholder">Ваше ім'я</div>
					<input type="text" name="country" id="country" placeholder="Ваше ім'я" size="60" class="input-text" onkeyup="placeholderInput($(this))"/>
					<div class="form-error errorName">Введіть Ваше ім`я</div>
				</div>
				<div class="div-input col-form">
					<div class="input-placeholder">Бюджет</div>
					<input type="text" name="budget" id="budget" placeholder="Бюджет" size="60" class="input-text" onkeyup="placeholderInput($(this))"/>
				</div>
			</div>
			<div class="box-modal-block-form">
				<div class="div-input col-form">
					<div class="input-placeholder">Кількість людей</div>
					<input type="text" name="number_people" id="number_people" placeholder="Кількість людей" size="20" class="input-text" onkeyup="placeholderInput($(this))" />
				</div>
				<div class="div-input col-form">
					<div class="input-placeholder">Кількість днів</div>
					<input type="text" name="number_days" id="number_days" placeholder="Кількість днів" size="60" class="input-text" onkeyup="placeholderInput($(this))"/>
				</div>
			</div>
			<div class="div-input">
				<div class="input-placeholder">Введіть Ваш Телефон</div>
				<input type="text" name="phone" id="phone" placeholder="Введіть Ваш Телефон" size="20" class="input-text" onkeyup="placeholderInput($(this))" />
				<div class="form-error errorPhone">Неправильний формат телефону</div>
			</div>
			
			<div class="box-modal-block-form">
				<div class="div-input col-form">
				<div class="input-placeholder">Введіть Вашу Почту</div>
				<input type="text" name="email" id="email" placeholder="E-mail" size="20" class="input-text" onkeyup="placeholderInput($(this))" />
				<div class="form-error erroreMail">Введіть Вашу електрону адресу</div>
			</div>
				<div class="div-input col-form">
					<div class="input-placeholder z-index">Місто</div>
					<select class="js-example-basic-single-limit" id="select-town">
						<option></option>																	
					</select>
					<div class="form-error erroreSelect">Оберіть Ваш Місто</div>
				</div>
			</div>
			

			<input type="button" class="red-button call_modal" value="Відправити">
			<div class="selection-checkbox" id="selection-check-error">
		<input type="checkbox" name="selectionTourForm" class="selection-form-checkbox" id="selection-check" checked><label for="selection-check" class="p-form-checkbox"><span class="span-form-checkbox">Надаю згоду на обробку моїх персональних даних згідно Закону України «Про захист персональних даних»</span></label>
			</div>
		</div>
</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js">   	
	</script>

	<!-- Owl Carousel script -->
	<script type="text/javascript" src="new-landing/libs/owlcarousel/owl.carousel.min.js">
	</script>

	<!-- arcticModal script -->
	<script type="text/javascript" src="new-landing/libs/arcticmodal/jquery.arcticmodal-0.3.min.js">
	</script>

	<!-- arcticModal style --> 
	<link href="/new-landing/libs/arcticmodal/jquery.arcticmodal-0.3.css" rel="stylesheet">

	<!-- font-awesome script -->
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

	<!-- Select2 js -->
	<script type="text/javascript" src="new-landing/libs/select2-master/select2.full.min.js">		
	</script>
	
	<!-- Script -->
	<script type="text/javascript" src="new-landing/js/script.js">
	</script>
    
  </body>
</html>